// Unit tests for SQLHelper
import 'package:test/test.dart';
import 'package:sqflite/sqflite.dart';
import '../lib/SQLHelper.dart';
import 'dart:io';

final SQLHelper _helper = new SQLHelper();

void main() {
  test('Database creation test', () {
    _helper.installDb().then((file) {
      expect(FileSystemEntityType.FILE, FileSystemEntity.typeSync(file.path));
    });
  });

  test('Database read test', () {
    _helper.getDb().then((sql) async {
      Database db = await openDatabase(sql.path);
      List<Map> list = await db.rawQuery('SELECT * FROM spell WHERE _id = 1');
      expect(1, list.length);
    });
  });
}