/*
  This file will hold the BottomNav logic for the main view(:
 */

 import 'package:flutter/material.dart';
 import 'SpellsView.dart';
 import 'CharacterView.dart';
 import 'InventoryView.dart';
 import 'SpellDetailsView.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Dungeoneer',
      theme: new ThemeData(
        primarySwatch: Colors.green,
      ),
      home: new BottomNavigation(),
    );
  }
}
 
 class NavigationIconView {
   NavigationIconView({
     Widget icon,
     Widget title,
     StatefulWidget view,
     Color color,
     TickerProvider vsync,
   }) : _icon = icon,
        _color = color,
        _view = view,
      item = new BottomNavigationBarItem(
          icon: icon,
          title: title,
          backgroundColor: color,
      ),
      controller = new AnimationController(
        duration: kThemeAnimationDuration,
        vsync: vsync,
      ) {
     _animation = new CurvedAnimation(
         parent: controller,
         curve: const Interval(0.5, 1.0, curve: Curves.fastOutSlowIn),
     );
   }

   final Widget _icon;
   final StatefulWidget _view;
   final Color _color;
   final BottomNavigationBarItem item;
   final AnimationController controller;
   CurvedAnimation _animation;

   FadeTransition transition(BottomNavigationBarType type, BuildContext context) {
     Color iconColor;
     if (type == BottomNavigationBarType.shifting) {
       iconColor = _color;
     } else {
       final ThemeData themeData = Theme.of(context);
       iconColor = themeData.brightness == Brightness.light
            ? themeData.primaryColor
            : themeData.accentColor;
     }

     return new FadeTransition(
          opacity: _animation,
          child: new SlideTransition(
            position: new Tween<Offset>(
              begin: const Offset(0.0, 0.02),
              end: Offset.zero,
            ).animate(_animation),
            child: new IconTheme(
              data: new IconThemeData(
                color: iconColor,
                size: 120.0,
              ),
              child: _view,
            ),
          ),
     );
   }
 }

 class BottomNavigation extends StatefulWidget {
   static const String routeName = '/material/bottom_navigation';

   @override
   _BottomNavigationState createState() => new _BottomNavigationState();
 }

 class _BottomNavigationState extends State<BottomNavigation>
     with TickerProviderStateMixin {
   //Start on the middle item
   int _currentIndex = 1;
   BottomNavigationBarType _type = BottomNavigationBarType.shifting;
   List<NavigationIconView> _navigationViews;

   @override
   void initState() {
     super.initState();
     //The items in the bottomnav
     _navigationViews = <NavigationIconView>[
       new NavigationIconView(
         icon: const Icon(Icons.book),
         title: const Text('Spellbook'),
         color: Colors.green,
         vsync: this,
         view: new SpellsView(),
       ),
       new NavigationIconView(
         icon: const Icon(Icons.portrait),
         title: const Text('Character'),
         color: Colors.greenAccent,
         vsync: this,
         view: new CharacterView(),
       ),
       new NavigationIconView(
         icon: const Icon(Icons.inbox),
         title: const Text('Inventory'),
         color: Colors.green,
         vsync: this,
         view: new InventoryView(),
       )
     ];
     //This attaches the rebuild method to the navigation items
     for (NavigationIconView view in _navigationViews)
       view.controller.addListener(_rebuild);

     _navigationViews[_currentIndex].controller.value = 1.0;
   }

   @override
   void dispose() {
     for (NavigationIconView view in _navigationViews)
       view.controller.dispose();
     super.dispose();
   }

   //When this method is called the UI will simply refresh to animate views
   //See the build method below
   void _rebuild() {
     setState(() {});
   }

   /*
      This function switches the icon in the 'body' between the action bar
      and the bottomnavbar. We add the transitions to a list and sort them
      based on which one is selected now, and return a Stack of transitions
    */
   Widget _buildTransitionsStack() {
     final List<FadeTransition> transitions = <FadeTransition>[];

     for (NavigationIconView view in _navigationViews)
       transitions.add(view.transition(_type, context));

     // Put the fading in views on top
     transitions.sort((FadeTransition a, FadeTransition b) {
       final Animation<double> aAnimation = a.listenable;
       final Animation<double> bAnimation = b.listenable;
       final double aValue = aAnimation.value;
       final double bValue = bAnimation.value;
       return aValue.compareTo(bValue);
     });

     return new Stack(children: transitions);
   }

   @override
   Widget build(BuildContext context) {
     final BottomNavigationBar botNavBar = new BottomNavigationBar(
       items: _navigationViews
         .map((NavigationIconView navigationView) => navigationView.item)
         .toList(),
       currentIndex: _currentIndex,
       type: _type,
       onTap: (int index) {
         //When an item is tapped on the navbar we call setState and:
         setState(() {
           //Play the 'deselect' animation on the current item
           _navigationViews[_currentIndex].controller.reverse();
           //Change the current item to the new selected item
           _currentIndex = index;
           //And play the select animation on the new item
           _navigationViews[_currentIndex].controller.forward();
         });
       },
     );

     return new Scaffold(
       appBar: new AppBar(
         title: const Text('Dungeoneer'),
         actions: <Widget>[
           new PopupMenuButton<BottomNavigationBarType>(
             onSelected: (BottomNavigationBarType value) {
               //Here we call setState and set the type for the bottomnav
               //This will be removed once we decide on which to use
               setState(() {
                 _type = value;
               });
             },
             //Add the navbartype toggles to the overflow menu
               //It'll pass the value item to the onSelected method above
             itemBuilder: (BuildContext context) => <PopupMenuItem<BottomNavigationBarType>>[
               const PopupMenuItem<BottomNavigationBarType>(
                 value: BottomNavigationBarType.fixed,
                 child: const Text('Fixed'),
               ),
               const PopupMenuItem(
                 value: BottomNavigationBarType.shifting,
                 child: const Text('shifting'),
               )
             ],
           )
         ],
       ),
       body: new Center(
            //Here is where we can switch the views in the 'body'
           child: _buildTransitionsStack()
       ),
       //Finally attach the bottomNavBar object
       bottomNavigationBar: botNavBar,
     );
   }
 }