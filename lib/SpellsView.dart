// This class holds the logic for the Spells list view
//Spells are pulled from the sqlite database and populated at runtime

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'SQLHelper.dart';
import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'SpellDetailsView.dart';

class SpellsView extends StatefulWidget {
  SpellsView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SpellsViewState createState() => new _SpellsViewState();
}

class _SpellsViewState extends State<SpellsView> with SingleTickerProviderStateMixin {
  SQLHelper sqlHelper = new SQLHelper();
  List<Map> objects = [];
  String _listSort;

  int _angle = 90;
  bool _isRotated = true;

  AnimationController _controller;
  Animation<double> _animation;
  Animation<double> _animation2;
  Animation<double> _animation3;

  @override
  void initState() {
    sqlHelper.isInstalled().then((dbInstalled) {
      if (!dbInstalled) {
        sqlHelper.installDb().then((future) {
          this.setState(() {});
        });
      }
    });
    _listSort = _listSort == null
        ? "SELECT * FROM spell ORDER BY level ASC" : _listSort;
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 180),
    );
    _animation = new CurvedAnimation(
      parent: _controller,
      curve: new Interval(0.0, 1.0, curve: Curves.linear),
    );
    _animation2 = new CurvedAnimation(
      parent: _controller,
      curve: new Interval(0.5, 1.0, curve: Curves.linear),
    );
    _animation3 = new CurvedAnimation(
      parent: _controller,
      curve: new Interval(0.8, 1.0, curve: Curves.linear),
    );
    _controller.reverse();
    super.initState();
  }

  void _rotate() {
    setState(() {
      if (_isRotated) {
        _angle = 45;
        _isRotated = false;
        _controller.forward();
      } else {
        _angle = 90;
        _isRotated = true;
        _controller.reverse();
      }
    });
  }

  void _sortList(String query) {
    //This will update the state with new sorting on the SQL database
    setState(() {
      _listSort = query;
    });
  }

  void _showSpellsDetail(Map spell) {
    Navigator.of(context).push(new MaterialPageRoute(
      builder: (BuildContext context) {
        return details(spell);
      },
    ));
  }

  Future _prepareSpell(int Id) async {
    Database database = await sqlHelper.getDb();
    database.rawUpdate("UPDATE spell SET favorite=1 WHERE _id = $Id");

  }

  //Called by the FutureBuilder in the build method
  Future<List> loadNext() async {
    Database database = await sqlHelper.getDb();
    List<Map> fetched = await database.rawQuery(_listSort);
    return fetched;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      floatingActionButton: multiLevelFAB(),
      body: new FutureBuilder(
        future: loadNext(),
        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
          if (!snapshot.hasData)
            return new Container();
          List content = snapshot.data;
          return new ListView.builder(
            scrollDirection: Axis.vertical,
            padding: new EdgeInsets.all(0.0),
            itemCount: content.length,
            itemBuilder: (BuildContext context, int index) {
              Widget favorite = content[index]['favorite'] == 1 ?
                  new Icon(Icons.star, color: Colors.yellow, size: 32.0) :
                  new Icon(Icons.star_border, size: 32.0);
              String level = content[index]['level'] == -1
                  ? 'CAN' : '${content[index]['level']}';
              return new Card(
                elevation: 3.0,
                child: new Center(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new ListTile(
                        onTap: () {
                          _showSpellsDetail(content[index]);
                        },
                        onLongPress: () {
                          _prepareSpell(content[index]['_id']);
                        },
                        title: new Text('${content[index]['name']}'),
                        subtitle: new Text(level),
                        leading: new CircleAvatar(
                          backgroundColor: Colors.green.shade400,
                          child: new Text('SP'),
                        ),
                        trailing: favorite,
                      ),
                    ],
                  ),
                )
              );
            },
          );
        }
      ),
    );
  }

  //This is what hell is like
  Stack multiLevelFAB() {
    return new Stack(
      children: <Widget>[
        new Positioned(
          bottom: 200.0,
          right: 14.0,
          child: new Container(
            child: new Row(
              children: <Widget>[
                new ScaleTransition(
                  scale: _animation3,
                  alignment: FractionalOffset.center,
                  child: new Container(
                    margin: new EdgeInsets.only(right: 16.0),
                    child: new Text(
                      'Prepared',
                      style: new TextStyle(
                        fontSize: 13.0,
                        fontFamily: 'Roboto',
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                new ScaleTransition(
                  scale: _animation3,
                  alignment: FractionalOffset.center,
                  child: new Material(
                    color: Colors.lightGreen,
                    type: MaterialType.circle,
                    elevation: 6.0,
                    child: new GestureDetector(
                      child: new Container(
                        width: 40.0,
                        height: 40.0,
                        child: new InkWell(
                          onTap: () {
                            if(_angle == 45.0){
                              //Change sorting to only pick prepared
                              _sortList("SELECT * FROM spell WHERE favorite=1 ORDER BY level ASC");
                            }
                          },
                          child: new Center(
                            child: new Icon(
                              Icons.grade,
                              size: 16.0,
                              color: Colors.yellow,
                            ),
                          ),
                        )
                      ),
                    )
                  ),
                ),
              ],
            ),
          )
        ),
        new Positioned(
          bottom: 144.0,
          right: 14.0,
          child: new Container(
            child: new Row(
              children: <Widget>[
                new ScaleTransition(
                  scale: _animation2,
                  alignment: FractionalOffset.center,
                  child: new Container(
                    margin: new EdgeInsets.only(right: 16.0),
                    child: new Text(
                      'Spellbook',
                      style: new TextStyle(
                        fontSize: 13.0,
                        fontFamily: 'Roboto',
                        color: Colors.grey,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                new ScaleTransition(
                  scale: _animation2,
                  alignment: FractionalOffset.center,
                  child: new Material(
                    color: Colors.lightGreen,
                    type: MaterialType.circle,
                    elevation: 6.0,
                    child: new GestureDetector(
                      child: new Container(
                        width: 40.0,
                        height: 40.0,
                        child: new InkWell(
                          onTap: () {
                            if(_angle == 45.0){
                              _sortList("SELECT * FROM spell ORDER BY level ASC");
                            }
                          },
                          child: new Center(
                            child: new Icon(
                              Icons.book,
                              size: 16.0,
                              color: Colors.yellow,
                            ),
                          ),
                        )
                      ),
                    )
                  ),
                ),
              ],
            ),
          )
        ),
        new Positioned(
          bottom: 88.0,
          right: 14.0,
          child: new Container(
            child: new Row(
              children: <Widget>[
                new ScaleTransition(
                  scale: _animation,
                  alignment: FractionalOffset.center,
                  child: new Container(
                    margin: new EdgeInsets.only(right: 16.0),
                    child: new Text(
                      'All Spells',
                      style: new TextStyle(
                        fontSize: 13.0,
                        fontFamily: 'Roboto',
                        color: new Color(0xFF9E9E9E),
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                new ScaleTransition(
                  scale: _animation,
                  alignment: FractionalOffset.center,
                  child: new Material(
                    color: Colors.lightGreen,
                    type: MaterialType.circle,
                    elevation: 6.0,
                    child: new GestureDetector(
                      child: new Container(
                        width: 40.0,
                        height: 40.0,
                        child: new InkWell(
                          onTap: (){
                            if(_angle == 45.0){
                              _sortList("SELECT * FROM spell ORDER BY name ASC");
                            }
                          },
                          child: new Center(
                            child: new Icon(
                              Icons.collections_bookmark,
                              size: 16.0,
                              color: Colors.yellow,
                            ),
                          ),
                        )
                      ),
                    )
                  ),
                ),
              ],
            ),
          )
        ),
        new Positioned(
          bottom: 8.0,
          right: 8.0,
          child: new Material(
            color: Colors.lightGreen,
            type: MaterialType.circle,
            elevation: 6.0,
            child: new GestureDetector(
              child: new Container(
                width: 56.0,
                height: 56.00,
                child: new InkWell(
                  onTap: _rotate,
                  child: new Center(
                    child: new Icon(
                        Icons.filter_list,
                        size: 24.0,
                        color: Colors.white,
                    )
                  ),
                )
              ),
            )
          ),
        )
      ]
    );
  }
}
