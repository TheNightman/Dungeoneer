import 'package:flutter/material.dart';

class InventoryView extends StatefulWidget {
  InventoryView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _InventoryViewState createState() => new _InventoryViewState();
}

class _InventoryViewState extends State<InventoryView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Text('Inventory list goes here'),
      )
    );
  }
}