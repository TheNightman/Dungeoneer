import 'package:flutter/material.dart';

class CharacterView extends StatefulWidget {
  CharacterView({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CharacterViewState createState() => new _CharacterViewState();
}

class _CharacterViewState extends State<CharacterView> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
        child: new Text('Character View'),
      ),
    );
  }
}
