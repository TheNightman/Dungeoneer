// This file holds helper classes for working with the sqflite package
// It might not work idk let's find out
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

class SQLHelper {
  //This method installs the db file from assets into the device filesystem
  Future installDb() async {
    Directory docsDir = await getApplicationDocumentsDirectory();
    String dbPath = join(docsDir.path, "final_spells.db");

    //delete if the db exists
    await deleteDatabase(dbPath);

    //Copy from assets
    ByteData data = await rootBundle.load(join("assets", "final_spells.db"));
    List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await new File(dbPath).writeAsBytes(bytes);
  }

  //Will return true if the database is installed to the DocumentsDirectory
  Future<bool> isInstalled() async {
    Directory docsDir = await getApplicationDocumentsDirectory();
    return FileSystemEntity.isFile(join(docsDir.path, "final_spells.db"));
  }

  //This method will return a Database object from the db file in the fs
  Future<Database> getDb() async {
    /**
     *  TODO: How can we get this without calling
     *  getApplicationDocumentsDirectory a second time?
     **/
    Directory docsDir = await getApplicationDocumentsDirectory();
    String dbPath = join(docsDir.path, "final_spells.db");

    return await openDatabase(dbPath);
  }

}