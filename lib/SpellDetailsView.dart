/*
    This file holds the *Scaffold* for the Spell Details View. This is still a
    new thing so there may be a better way, but for now the best way I can tell
    to show the view is just by using Navigator.push to switch to a new view
    with this scaffold. Since it's only showing details of a spell and shouldn't
    require too much user interaction this should be fine for now until I figure
    out if there's a more `proper` way to do it.
 */
import 'package:flutter/material.dart';

Scaffold details(Map spell) {
  return new Scaffold(
    appBar: new AppBar(
      title: new Text(spell['name']),
    ),
    body: new SingleChildScrollView(
      child: new Column(
        children: <Widget>[
          new Padding(
            padding: new EdgeInsets.only(top: 18.0),
            child: new Wrap(
              spacing: 8.0,
              runSpacing: 6.0,
              children: <Widget>[
                new Chip(label: new Text('${spell['casting_time']}')),
                new Chip(label: new Text('${spell['range']}')),
                new Chip(label: new Text('${spell['components']}')),
                new Chip(label: new Text('${spell['duration']}')),
              ]
            ),
          ),
          new Padding(
            padding: new EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 12.0),
            child: new Text(spell['description']),
          ),
        ],
        mainAxisSize: MainAxisSize.max,
      ),
    ),
  );
}